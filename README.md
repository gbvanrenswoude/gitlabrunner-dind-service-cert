To use the docker in docker service in gitlabrunner (.gitlab-ci.yml) icm private and internal hosted registries you can do the following:
```
variables:
  NO_PROXY: "169.254.169.254,127.0.0.1,localhost,docker"
  $SCRIPT_DIR: something

somebuild:
  stage: build
  image: docker:stable
  environment:
    name: development
  services:
    - name: docker:dind
      entrypoint: []
      command:              # make the dind service work with an internal cert in this case for artifactory prd
        - /bin/sh
        - -c
        - |
          mkdir -p /etc/docker/certs.d/registry.artifactory.prd.somecompany.com;
          echo "
          $ARTIFACTORY_CER
          " > /etc/docker/certs.d/registry.artifactory.prd.somecompany.com/ca.crt;
          dockerd-entrypoint.sh
        - echo "done"
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_HOST: tcp://docker:2375/
  script:
    - source $SCRIPT_DIR/build_some_image.sh
```

add in the gitlab variables the cert in ARTIFACTORY_CER or just paste it in the place of the variable.

In the build_some_image you can auth to your registry and do the usual docker build docker push. For an example working with ecr see the ecr_build_some_image.sh script in the root of this project.
```
telinit 0
```