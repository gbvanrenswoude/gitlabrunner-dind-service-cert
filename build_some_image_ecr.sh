#!/bin/sh

version=$(curl some stuff)
apk add --no-cache python3
python3 -m ensurepip
pip3 install --upgrade pip setuptools
pip install awscli
cd ohpen-api-spring
$(aws ecr get-login --no-include-email --region eu-west-1)
docker build --build-arg http_proxy=http://somecompany.com:8080 --build-arg https_proxy=http://somecompany.com:8080 -t some_api .
docker tag some_api:latest 111111111111.dkr.ecr.eu-west-1.amazonaws.com/some_api:$version
docker push 111111111111.dkr.ecr.eu-west-1.amazonaws.com/some_api:$version
